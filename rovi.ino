#include <Servo.h>

#define TRIM_L 100
#define TRIM_R 0

#define PIN_PWMB 6
#define PIN_PWMA 7
#define PIN_OUT1 8
#define PIN_OUT2 9


//// Assign your channel in pins
#define STEERING_IN_PIN 2
#define THROTTLE_IN_PIN 3
#define MODE_IN_PIN 4
#define REVERSE_IN_PIN 5

//left throttle and right throttle boundaries
#define MAX_L 2200
#define MAX_R 2000
#define MIN_L 1100
#define MIN_R 900

//variable declarations
Servo pwma;
Servo pwmb;
long throttleLeft = 1400;
long throttleRight = 1400;
long throttle = 1400;
long steering = 1500;
long mode = 0;

void setup() {
  Serial.begin(9600);

  //pin initialization
  pinMode(THROTTLE_IN_PIN, INPUT);
  pinMode(STEERING_IN_PIN, INPUT);
  pinMode(MODE_IN_PIN, INPUT);
  pinMode(REVERSE_IN_PIN, INPUT);
  pinMode(PIN_OUT1, OUTPUT);
  pinMode(PIN_OUT2, OUTPUT);

  //set stop
  digitalWrite(PIN_OUT1, LOW);
  digitalWrite(PIN_OUT2, LOW);
  //PWM initialization
  pwmb.attach(PIN_PWMA);
  pwma.attach(PIN_PWMB);
  pwma.writeMicroseconds(1000);
  pwmb.writeMicroseconds(1000);

}

//drive based on steering s and throttle t inputs
void drive(long s, long t) {


  float center = 1500;

  //P * (s - center)
  float d = 0.8 * (s - center);
  //set steering
  steering = center + d;

  //set throttleR/L = throttle -/+ P*d + trimL/R
  throttleLeft = t + 2*d + TRIM_L;
  throttleRight = t - 1.8*d + TRIM_R;

  //set boundaries
  if (throttleLeft >= MAX_L) {
    throttleLeft = MAX_L;
  }
  if (throttleRight >= MAX_R) {
    throttleRight = MAX_R;
  }
  if (throttleLeft <= MIN_L) {
    throttleLeft = MIN_L;
  }
  if (throttleRight <= MIN_R) {
    throttleRight = MIN_R;
  }
}


//manual mode drive
void manual( long in_throttle,  long in_steering, bool reverse) {

  //throttle threshold
  if (in_throttle > 1100) {

    drive(in_steering, in_throttle);

    if (reverse) {
      digitalWrite(PIN_OUT1, LOW);
      digitalWrite(PIN_OUT2, HIGH);
    } else {
      digitalWrite(PIN_OUT1, HIGH);
      digitalWrite(PIN_OUT2, LOW);
    }
    pwma.writeMicroseconds(throttleLeft);
    pwmb.writeMicroseconds(throttleRight);
  } else {
    //stopped
    throttleLeft = throttleRight = 1000;
    digitalWrite(PIN_OUT1, LOW);
    digitalWrite(PIN_OUT2, LOW);
  }
}

void loop() {
  //retrieve input from remote receiver
  unsigned int start = micros();
  long   in_throttle = pulseIn(THROTTLE_IN_PIN, HIGH,10000);
  long   in_steering =  pulseIn(STEERING_IN_PIN, HIGH,10000);
  long   mod =  pulseIn(MODE_IN_PIN, HIGH,10000);
  static bool reverse = false;
  long    rev = pulseIn(REVERSE_IN_PIN, HIGH,10000);

  static long s, t, r, m;


  if (mod > 0){
    m = mod;
  }
  if (rev > 0){
    r = rev;
  }
  //set throtle value
  if (in_throttle > 0){
    t = in_throttle;
    throttle = t;
  }

  //set steering value
  if (in_steering > 0){
    s = in_steering;
  }
  
  //change mode
  if (m> 1500) {
    mode = 1;
  } else if (m > 0) {
    mode = 0;
  }

 

  //set reverse or forward
  if (r > 1500) {
    reverse = true;
  } else if (r > 0) {
    reverse = false;
  }


  //drive rover
  manual(t, s, reverse);


  Serial.print(throttle);
  Serial.print("\t");
  Serial.print(throttleLeft);
  Serial.print("\t");
  Serial.println(throttleRight);




  //  Serial.print(steering);
  //  Serial.print("\t");
  //  Serial.print(throttle);
  //  Serial.print("\t");
  //  Serial.println(mode);
  unsigned int delta = micros() - start;
  if (delta < 40000) {
    delay((40000 - delta) / 1000);
  }

}
